//
//  ScoreFeedbackView.swift
//  Rechoir
//
//  Created by Chondro Satrio Wibowo on 30/07/21.
//

import SwiftUI
import AVKit

struct ScoreFeedbackView: View {
    
    var linkAudio:URL?
    @State var audioPlayer:AVAudioPlayer!
    @State var audioPlayingBool:Bool = true
    @State var isPlayingRecord:Bool = false
    //@StateObject private var orientationInfo:OrientationInfoViewmodel = OrientationInfoViewmodel()
    
    
    var body: some View {
        GeometryReader{geometry in
            if geometry.size.height < geometry.size.width{
                VStack{
                    Rectangle().frame(height:20).foregroundColor(.white)
                    Text("Oh no! You missed a lot on pitch and tempo. Your score is").font(.system(size: 17))
                    Text("45/100").font(.system(size: 35)).foregroundColor(.red).bold()
                    Text("You need to redo your recoding until you hit a minimum score of 90.")
                    Image("musicscore").resizable().frame(width:538,height: 118).font(.system(size: 17))
                    
                    HStack{
                        NavigationLink(destination: LiveRecordingView(), label: {
                            
                            VStack{
                                Image(systemName: "arrow.counterclockwise").font(.system(size: 32)).foregroundColor(.init(Color("PrimaryColor")))
                                Text("Retry").foregroundColor(.init(Color("PrimaryColor")))
                            }.padding(.horizontal,30)
                            
                            
                        })
                        
                        Button(action: {
                            do{
                                
                                if audioPlayingBool{
                                    self.audioPlayer = try! AVAudioPlayer(contentsOf: linkAudio!)
                                    self.audioPlayer.play()
                                    audioPlayingBool.toggle()
                                    print(audioPlayingBool)
                                }
                                else{
                                    self.audioPlayer = try! AVAudioPlayer(contentsOf: linkAudio!)
                                    self.audioPlayer.volume = 1.0
                                    self.audioPlayer.stop()
                                    audioPlayingBool.toggle()
                                }
                                
                                
                                
                            }
                            catch _{
                                
                            }
                        }, label: {
                            VStack{
                                Image(systemName: audioPlayingBool ? "play.fill":"stop.fill").font(.system(size: 32)).foregroundColor(audioPlayingBool ?.init(Color("PrimaryColor")):.red)
                                Text(audioPlayingBool ? "Play":"Stop").foregroundColor(audioPlayingBool ?.init(Color("PrimaryColor")):.red)
                            }
                            
                        }).padding(.horizontal,30)
                        
                        
                        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                            VStack{
                                Image(systemName: "square.and.arrow.up").font(.system(size: 32)).foregroundColor(.init(Color("PrimaryColor")))
                                Text("Upload").foregroundColor(.init(Color("PrimaryColor")))
                            }
                            
                        }).padding(.horizontal,30)
                    }
                }.navigationBarItems(trailing:NavigationLink(destination: DetailMusicRecordView(), label: { Image(systemName: "music.note.list").foregroundColor(.white).font(.title2)}) ).navigationBarTitle("Kasih Ibu Alto",displayMode: .inline)
            }
            else{
                VStack{
                    Rectangle().frame(height:20).foregroundColor(.white)
                    Text("Oh no! You missed a lot on pitch and tempo. Your score is").font(.system(size: 17)).frame(width: 286).multilineTextAlignment(.center)
                    Text("45/100").font(.system(size: 35)).foregroundColor(.red).bold()
                    Text("You need to redo your recoding until you hit a minimum score of 90.").frame(width: 286).multilineTextAlignment(.center)
                    Image("potraitMusicScore").frame(width: 352, height: 456, alignment: .center)
                    HStack{
                        NavigationLink(destination: LiveRecordingView(), label: {
                            
                            VStack{
                                Image(systemName: "arrow.counterclockwise").font(.system(size: 32)).foregroundColor(.init(Color("PrimaryColor")))
                                Text("Retry").foregroundColor(.init(Color("PrimaryColor")))
                            }.padding(.horizontal,30)
                            
                            
                        })
                        
                        
                        Button(action: {
                            do{
                                
                                if audioPlayingBool{
                                    self.audioPlayer = try! AVAudioPlayer(contentsOf: linkAudio!)
                                    self.audioPlayer.play()
                                    audioPlayingBool.toggle()
                                    print(audioPlayingBool)
                                }
                                else{
                                    self.audioPlayer = try! AVAudioPlayer(contentsOf: linkAudio!)
                                    self.audioPlayer.volume = 1.0
                                    self.audioPlayer.stop()
                                    audioPlayingBool.toggle()
                                }
                                
                                
                                
                            }
                            catch _{
                                
                            }
                        }, label: {
                            VStack{
                                Image(systemName: audioPlayingBool ? "play.fill":"stop.fill").font(.system(size: 32)).foregroundColor(audioPlayingBool ?.init(Color("PrimaryColor")):.red)
                                Text(audioPlayingBool ? "Play":"Stop").foregroundColor(audioPlayingBool ?.init(Color("PrimaryColor")):.red)
                            }
                            
                        }).padding(.horizontal,30)
                        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                            VStack{
                                Image(systemName: "square.and.arrow.up").font(.system(size: 32)).foregroundColor(.init(Color("PrimaryColor")))
                                Text("Upload").foregroundColor(.init(Color("PrimaryColor")))
                            }
                            
                        }).padding(.horizontal,30)
                    }
                    
                }.navigationBarItems(trailing:NavigationLink(destination: DetailMusicRecordView(), label: { Image(systemName: "music.note.list").foregroundColor(.white).font(.title2)}) ).navigationBarTitle("Kasih Ibu Alto",displayMode: .inline).onAppear(perform: {}).onDisappear(perform: {
                })
            }
        }
        
        
        
    }
}

struct ScoreFeedbackView_Previews: PreviewProvider {
    static var previews: some View {
        ScoreFeedbackView()
    }
}
