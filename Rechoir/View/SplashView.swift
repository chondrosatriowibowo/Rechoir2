//
//  SplashView.swift
//  Rechoir
//
//  Created by Chondro Satrio Wibowo on 29/07/21.
//

import SwiftUI

struct SplashView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct SplashView_Previews: PreviewProvider {
    static var previews: some View {
        SplashView()
    }
}
