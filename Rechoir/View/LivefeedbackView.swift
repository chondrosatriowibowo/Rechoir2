//
//  LivefeedbackView.swift
//  Rechoir
//
//  Created by Chondro Satrio Wibowo on 27/07/21.
//

import SwiftUI

struct LivefeedbackView: View {
    var list:[String] = ["Sopran","Alto","Tenor","Bass"]
    var body: some View {
        ScrollView{
            VStack{
                Image("kasihibumusic").resizable().frame(width:290,height:410)
                
                ForEach(list,id:\.self){text in
                    NavigationLink(
                        destination: RecordView(title: text),
                        label: {
                            HarmonyComponents(text: text)
                        })
                    
                    
                    
                }
            }
        }
        
        
        .navigationBarTitle("Kasih Ibu")
    }
}

struct LivefeedbackView_Previews: PreviewProvider {
    static var previews: some View {
        LivefeedbackView()
    }
}
