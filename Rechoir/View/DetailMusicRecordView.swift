//
//  DetailMusicRecordView.swift
//  Rechoir
//
//  Created by Chondro Satrio Wibowo on 26/07/21.
//

import SwiftUI



struct DetailMusicRecordView: View {
    @StateObject var data = AudioAnalys()
    @StateObject var playMusic = PlayerMusicViewModel()
    
    init() {
        Theme.navigationBarColors(background: UIColor.init(Color("PrimaryColor")),titleColor: UIColor.white)
    }
    var body: some View {
        
        ScrollView{
            VStack(alignment:.center){
                Spacer(minLength: 20)
                NavigationLink(
                    destination: LivefeedbackView(),
                    label: {
                        SongListTile(name: "Kasih Ibu", time: "0:33")
                    })
                
                NavigationLink(
                    destination: LivefeedbackView(),
                    label: {
                        SongListTile(name: "Never Enough", time: "2:50")
                    })
                NavigationLink(
                    destination: LivefeedbackView(),
                    label: {
                        SongListTile(name: "Ampar Ampar Pisang", time: "1:00")
                    })
                NavigationLink(
                    destination: LivefeedbackView(),
                    label: {
                        SongListTile(name: "I will never leave you", time: "4:00")
                    })
                NavigationLink(
                    destination: LivefeedbackView(),
                    label: {
                        SongListTile(name: "Reflection", time: "3:25")
                    })
                
            }
        }
        
        .navigationBarTitle("Song List",displayMode: .large).navigationBarHidden(false)
        .navigationBarBackButtonHidden(true)
        .navigationBarTitleDisplayMode(.large)
        
        
        
        
    }
}

struct DetailMusicRecordView_Previews: PreviewProvider {
    static var previews: some View {
        DetailMusicRecordView()
    }
}
