//
//  CobaCobaView.swift
//  Rechoir
//
//  Created by Wilfredo Sutanto on 02/08/21.
//

import SwiftUI

struct CobaCobaView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct CobaCobaView_Previews: PreviewProvider {
    static var previews: some View {
        CobaCobaView()
    }
}
