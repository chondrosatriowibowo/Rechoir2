//
//  Board2View.swift
//  Rechoir
//
//  Created by Chondro Satrio Wibowo on 29/07/21.
//

import SwiftUI

struct Board2View: View {
    var body: some View {
        VStack{
            Image("board2")
            Rectangle().frame(height:105).foregroundColor(.init(Color("PrimaryColor")))
            if #available(iOS 14.0, *) {
                Text("No more missing the beat").font(.title2).foregroundColor(.white).bold()
            } else {
                // Fallback on earlier versions
            }
            Rectangle().frame(height:6).foregroundColor(.init(Color("PrimaryColor")))
            Text("sing along with our easy to follow guide").font(.callout).foregroundColor(.white).frame(width:280,alignment: .center).multilineTextAlignment(.center)
        }.background(Rectangle().ignoresSafeArea().frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).foregroundColor(.init(Color("PrimaryColor"))))
    }
}

struct Board2View_Previews: PreviewProvider {
    static var previews: some View {
        Board2View()
    }
}
