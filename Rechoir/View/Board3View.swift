//
//  Board3View.swift
//  Rechoir
//
//  Created by Chondro Satrio Wibowo on 29/07/21.
//

import SwiftUI

struct Board3View: View {
    var body: some View {
        VStack{
            Image("board3")
            Rectangle().frame(height:105).foregroundColor(.init(Color("PrimaryColor")))
            Text("Get live pitch feedback").font(.title2).foregroundColor(.white).bold()
            Rectangle().frame(height:6).foregroundColor(.init(Color("PrimaryColor")))
            Text("you’ll see which pitch that you miss when you sing and record").font(.callout).foregroundColor(.white).frame(width:280,alignment: .center).multilineTextAlignment(.center)
            Rectangle().frame(height:12).foregroundColor(.init(Color("PrimaryColor")))
            
            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                NavigationLink(
                    destination: DetailMusicRecordView(),
                    label: {
                        Text("Continue").foregroundColor(.init(Color("PrimaryColor")))
                    })
                
            }).background(RoundedRectangle(cornerRadius: 8, style: /*@START_MENU_TOKEN@*/.continuous/*@END_MENU_TOKEN@*/).frame(width:100,height: 32).foregroundColor(.white)).padding()
        }.background(Rectangle().ignoresSafeArea().frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).foregroundColor(.init(Color("PrimaryColor"))))
    }
}

struct Board3View_Previews: PreviewProvider {
    static var previews: some View {
        Board3View()
    }
}
