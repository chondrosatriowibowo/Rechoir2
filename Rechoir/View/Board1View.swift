//
//  Board1View.swift
//  Rechoir
//
//  Created by Chondro Satrio Wibowo on 29/07/21.
//

import SwiftUI

struct Board1View: View {
    @available(iOS 14.0, *)
    var body: some View {
        if #available(iOS 14.0, *) {
            VStack{
                Image("board1")
                Rectangle().frame(width:100,height:100).foregroundColor(.init(Color("PrimaryColor")))
                Text("Listen and Record at the same time").font(.title2).foregroundColor(.white).bold().frame(width:220).multilineTextAlignment(.center)
                Rectangle().frame(height:6).foregroundColor(.init(Color("PrimaryColor")))
                Text("listen to your music guide, sing,and record at the same time!").font(.callout).foregroundColor(.white).frame(width:280,alignment: .center).multilineTextAlignment(.center)
            }.background(Rectangle().ignoresSafeArea().frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).foregroundColor(.init(Color("PrimaryColor"))))
        } else {
            // Fallback on earlier versions
        }
    }
}

struct Board1View_Previews: PreviewProvider {
    static var previews: some View {
        Board1View()
    }
}
