//
//  RotatePhoneView.swift
//  Rechoir
//
//  Created by Chondro Satrio Wibowo on 27/07/21.
//

import SwiftUI

struct RotatePhoneView: View {
    
    @State var rotation = 0.0
    var body: some View {
        VStack{
            Text("Rotate your phone").foregroundColor(.init(Color("PrimaryColor")))
                .padding()
            
            Image("ilustrationphone").animation(Animation.easeIn(duration: 4).speed(3)).rotationEffect(.degrees(rotation)).onAppear(){
                rotation += 270.0
            }
            NavigationLink(
                destination: LiveRecordingView(),
                label: {
                    
                    Image(systemName: "arrow.forward.circle.fill").foregroundColor(.init(Color("PrimaryColor"))).font(.system(size: 40)).padding()
                    
                })
            Text("Tap Arrow if you have rotate").font(.title3).padding()
            
        }
    }
}

struct RotatePhoneView_Previews: PreviewProvider {
    static var previews: some View {
        RotatePhoneView()
    }
}
