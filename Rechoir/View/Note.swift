//
//  Note.swift
//  Rechoir
//
//  Created by Chondro Satrio Wibowo on 30/07/21.
//

import Foundation

struct Note {
    var name:String
    var duration:Float
    var frequency:Float
}

struct Music{
    var name:String
    var note:[Note]
    var duration:Float
}



