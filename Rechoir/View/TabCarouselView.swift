//
//  TabCarouselView.swift
//  Rechoir
//
//  Created by Chondro Satrio Wibowo on 29/07/21.
//

import SwiftUI

extension View {
    func phoneOnlyStackNavigationView() -> some View {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return AnyView(self.navigationViewStyle(StackNavigationViewStyle()))
        } else {
            return AnyView(self)
        }
    }
}
struct TabCarouselView: View {
    var title:[String] = ["Listen and Record at the same time","No more missing the beat","Get live pitch feedback"]
    var desc:[String] = ["listen to your music guide, sing,and record at the same time!","sing along with our easy to follow guide","you’ll see which pitch that you miss when you sing and record"]
    init() {
        Theme.navigationBarColors(background: UIColor.init(Color("PrimaryColor")),titleColor: UIColor.white)
    }
    var body: some View {
        NavigationView{
            TabView{
                Board1View()
                Board2View()
                Board3View()
                
                
            }.tabViewStyle(PageTabViewStyle()).background(Rectangle().ignoresSafeArea().foregroundColor(.init(Color("PrimaryColor"))))
        }.accentColor(Color("PrimaryColor"))
        
    }
}

struct TabCarouselView_Previews: PreviewProvider {
    static var previews: some View {
        TabCarouselView()
    }
}
