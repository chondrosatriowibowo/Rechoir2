//
//  RecordView.swift
//  Rechoir
//
//  Created by Chondro Satrio Wibowo on 27/07/21.
//

import SwiftUI

struct RecordView: View {
    var title:String
    var body: some View {
        
        VStack{
            List(){
                ForEach(0..<1){ item in
                    NavigationLink(
                        destination: RotatePhoneView(),
                        label: {
                            RecordComponents()
                        })
                }
                
            }
            
            
            
        }.navigationBarTitle("Kasih Ibu -\(title)")
    }
}

struct RecordView_Previews: PreviewProvider {
    static var previews: some View {
        RecordView(title: "Sofran")
    }
}
