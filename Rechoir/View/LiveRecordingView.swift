//
//  LiveRecordingView.swift
//  Rechoir
//
//  Created by Chondro Satrio Wibowo on 29/07/21.
//

import SwiftUI
import AudioKit
import AVKit

struct LiveRecordingView: View {
    
    @StateObject var metronomeVM:PlayerMusicViewModel = PlayerMusicViewModel()
    //@ObservedObject var orientationInfo:OrientationInfoViewmodel = OrientationInfoViewmodel()
    
    //@State var isTapped = false
    @State var selection: Int? = nil
    @State var metronome:Bool = false
    var musicAlto = Music(name:"KasihIbuAlto",note: [Note(name: "G4", duration: 1000,frequency: 392.00),Note(name: "F4", duration: 500, frequency: 349.23),Note(name: "G4", duration: 500, frequency: 392.00),Note(name: "E4", duration: 1500, frequency: 329.63),Note(name: "E4", duration: 500, frequency: 329.63),Note(name: "F4", duration: 1000, frequency: 349.23),Note(name: "F4", duration: 500, frequency: 349.23),Note(name: "F4", duration: 500, frequency: 329.63),Note(name: "E4", duration: 2000, frequency: 349.23),Note(name: "C5", duration: 1000, frequency: 523.25),Note(name: "B4", duration: 500, frequency: 493.88),Note(name: "A4", duration: 500, frequency: 440.00),Note(name: "G4", duration: 1000, frequency: 392.00),Note(name: "E4", duration: 500, frequency: 329.63),Note(name: "F4", duration: 500, frequency: 349.23),Note(name: "G4", duration: 1000, frequency: 392.00),Note(name: "C5", duration: 1000, frequency: 523.25),Note(name: "B4", duration: 2000, frequency: 493.88),Note(name: "G4", duration: 500, frequency: 349.23),Note(name: "G4", duration: 500, frequency: 349.23),Note(name: "F4", duration: 500, frequency: 349.23),Note(name: "G4", duration: 500, frequency: 349.23),Note(name: "E4", duration: 1500, frequency: 329.63),Note(name: "E4", duration: 500, frequency: 329.63),Note(name: "F4", duration: 500, frequency: 349.23),Note(name: "F4", duration: 500, frequency: 349.23),Note(name: "F4", duration: 500, frequency: 349.23),Note(name: "F4", duration: 500, frequency: 349.23),Note(name: "E4", duration: 2000, frequency: 329.63),Note(name: "C5", duration: 500, frequency: 523.25),Note(name: "C5", duration: 500, frequency: 523.25),Note(name: "B4", duration: 500, frequency: 493.88),Note(name: "A4", duration: 500, frequency: 440.00),Note(name: "G4", duration: 1000, frequency: 392.00),Note(name: "E4", duration: 500, frequency: 329.63),Note(name: "F4", duration: 500, frequency: 349.23),Note(name: "G4", duration: 500, frequency: 392.00),Note(name: "G4", duration: 500, frequency: 392.00),Note(name: "E4", duration: 500, frequency: 329.63),Note(name: "F4", duration: 500, frequency: 349.23),Note(name: "E4", duration: 2000, frequency: 329.63)], duration: 32000)
    
    @State var detailRecord:AVAudioPlayer!
    @State private var isStopRecord:Bool = false
    @State private var isStopPlayGuide:Bool = false
    @State var record = false
    @State var audioPlay = false
    var a:Float = 0.0
    
    @State var session:AVAudioSession!
    @State var recorder:AVAudioRecorder!
    @State var audioPlayer:AVAudioPlayer!
    
    @State var alert = true
    
    @State var audios:[URL] = []
    @State var linkAudio:URL?
    static let ImageClick = Notification.Name.init("ImageClick")
    
    var audioRecord:FileManager?
    var timer:Timer?
    let timerAll = Timer.publish(every: 1.0, on: .main, in: .common).autoconnect()
    @State var currentTime = Date()
    
    @State var navigationScoreView:Bool = false
    let nc = NotificationCenter.default
    
    func mantap(){
        var a:Float = 0.0
        for i in 0..<musicAlto.note.count{
            let duration = musicAlto.note[i].duration
            
            a = a + duration
            print(a)
            
            
        }
        
        if a == musicAlto.duration{
            
        }
        print(a)
    }
    func VPNDidChangeStatus(_ notification:Notification){
        let audioRouteChangeReason = notification.userInfo![AVAudioSessionRouteChangeReasonKey] as! UInt
        
        switch audioRouteChangeReason {
        case AVAudioSession.RouteChangeReason.newDeviceAvailable.rawValue:
            print("headphone plugged in")
        case AVAudioSession.RouteChangeReason.oldDeviceUnavailable.rawValue:
            print("Headphone pulled out")
        default:
            break
        }
    }
    
    mutating func music(music:Music){
        var totalMusicNote = music.note.count
        if timer == nil{
            for i in 0..<musicAlto.note.count{
                let duration = musicAlto.note[i].duration
                
                a = (a + duration) / 1000
                print(a)
                
            }
        }
        RunLoop.current.add(timer!,forMode: .common)
        
    }
    
    init(){
        
        //mantap()
        
        
        
        
    }
    
    
    var body: some View {
        GeometryReader{geometry in
            if geometry.size.height < geometry.size.width{
                ZStack{
                    
                    if alert {
                        CustomAlertDialog(showAlert: $alert, width: UIScreen.main.bounds.width * 0.7 - 100, height: UIScreen.main.bounds.height * 0.8 - 80).frame(width: geometry.size.width, height: geometry.size.height, alignment: .center)
                    }
                    else{
                        VStack(alignment:.center){
                            
                            
                            HStack{
                                Spacer()
                                Text("60 Bpm")
                                Image(systemName: "metronome.fill").font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                                Toggle("", isOn:$metronome).frame(width:60).toggleStyle(SwitchToggleStyle(tint: Color("PrimaryColor"))).scaleEffect(x:0.8,y:0.8)
                            }.padding().frame( height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: .trailing).onChange(of: metronome, perform: { value in
                                if value{
                                    metronomeVM.data.isPlaying = value
                                    metronomeVM.start()
                                }
                                else{
                                    metronomeVM.stop()
                                }
                                
                            }).onDisappear(perform: {
                                metronomeVM.stop()
                            })
                            
                            Image("Groupas").resizable().frame(width:800,height: 200)
                            NavigationLink(destination: ScoreFeedbackView(linkAudio: linkAudio), tag: 1, selection: $selection){
                                if isStopRecord == false && isStopPlayGuide == false{
                                    if self.record == false && self.audioPlay == false{
                                        Button(action: {
                                            do{
                                                let currentRoute = AVAudioSession.sharedInstance().currentRoute
                                                
                                                if(currentRoute.outputs != nil){
                                                    for description in currentRoute.outputs{
                                                        if description.portType == AVAudioSession.Port.headphones{
                                                            print("Headphone plugged in")
                                                            let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                                                            
                                                            let fileName = url.appendingPathComponent("myRcd\(self.audios.count + 1).m4a")
                                                            print(fileName)
                                                            
                                                            linkAudio = url.appendingPathComponent("myRcd\(self.audios.count + 1).m4a")
                                                            
                                                            let settings = [
                                                                AVFormatIDKey:Int(kAudioFormatMPEG4AAC),AVSampleRateKey:12000,AVNumberOfChannelsKey:1,AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue]
                                                            
                                                            self.recorder = try! AVAudioRecorder(url: linkAudio!, settings:  settings)
                                                            self.recorder.record()
                                                            let sound = Bundle.main.path(forResource: "Kasih_Ibu_Alto.mp3", ofType: "mp3")
                                                            
                                                            self.audioPlayer = try! AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound!))
                                                            
                                                            self.audioPlayer.play()
                                                            self.audioPlay.toggle()
                                                            metronomeVM.data.isPlaying = true
                                                            metronomeVM.start()
                                                            self.metronome = true
                                                        }
                                                        else if description.portType == AVAudioSession.Port.headsetMic{
                                                            print("Headsetmic Output")
                                                            let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                                                            
                                                            let fileName = url.appendingPathComponent("myRcd\(self.audios.count + 1).m4a")
                                                            print(fileName)
                                                            
                                                            linkAudio = url.appendingPathComponent("myRcd\(self.audios.count + 1).m4a")
                                                            
                                                            let settings = [
                                                                AVFormatIDKey:Int(kAudioFormatMPEG4AAC),AVSampleRateKey:12000,AVNumberOfChannelsKey:1,AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue]
                                                            
                                                            self.recorder = try! AVAudioRecorder(url: linkAudio!, settings:  settings)
                                                            self.recorder.record()
                                                            let sound = Bundle.main.path(forResource: "Kasih_Ibu_Alto", ofType: "mp3")
                                                            
                                                            self.audioPlayer = try! AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound!))
                                                            
                                                            self.audioPlayer.play()
                                                            self.audioPlay.toggle()
                                                            metronomeVM.data.isPlaying = true
                                                            metronomeVM.start()
                                                            self.metronome = true
                                                            
                                                            
                                                        }
                                                        else if description.portType == AVAudioSession.Port.bluetoothHFP{
                                                            print("Bluetooth Output")
                                                            let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                                                            
                                                            let fileName = url.appendingPathComponent("myRcd\(self.audios.count + 1).m4a")
                                                            print(fileName)
                                                            
                                                            linkAudio = url.appendingPathComponent("myRcd\(self.audios.count + 1).m4a")
                                                            
                                                            let settings = [
                                                                AVFormatIDKey:Int(kAudioFormatMPEG4AAC),AVSampleRateKey:12000,AVNumberOfChannelsKey:1,AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue]
                                                            
                                                            self.recorder = try! AVAudioRecorder(url: linkAudio!, settings:  settings)
                                                            self.recorder.record()
                                                            let sound = Bundle.main.path(forResource: "Kasih_Ibu_Alto", ofType: "mp3")
                                                            
                                                            self.audioPlayer = try! AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound!))
                                                            
                                                            self.audioPlayer.play()
                                                            self.audioPlay.toggle()
                                                            metronomeVM.data.isPlaying = true
                                                            metronomeVM.start()
                                                            self.metronome = true
                                                            
                                                            
                                                        }
                                                        else if description.portType == AVAudioSession.Port.displayPort{
                                                            print("Display Output")
                                                            let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                                                            
                                                            let fileName = url.appendingPathComponent("myRcd\(self.audios.count + 1).m4a")
                                                            print(fileName)
                                                            
                                                            linkAudio = url.appendingPathComponent("myRcd\(self.audios.count + 1).m4a")
                                                            
                                                            let settings = [
                                                                AVFormatIDKey:Int(kAudioFormatMPEG4AAC),AVSampleRateKey:12000,AVNumberOfChannelsKey:1,AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue]
                                                            
                                                            self.recorder = try! AVAudioRecorder(url: linkAudio!, settings:  settings)
                                                            self.recorder.record()
                                                            let sound = Bundle.main.path(forResource: "Kasih_Ibu_Alto", ofType: "mp3")
                                                            
                                                            self.audioPlayer = try! AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound!))
                                                            
                                                            self.audioPlayer.play()
                                                            self.audioPlay.toggle()
                                                            metronomeVM.data.isPlaying = true
                                                            metronomeVM.start()
                                                            self.metronome = true
                                                            
                                                            
                                                        }
                                                        else if description.portType == AVAudioSession.Port.builtInSpeaker{
                                                            print("Speaker Output")
                                                            let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                                                            
                                                            let fileName = url.appendingPathComponent("myRcd\(self.audios.count + 1).m4a")
                                                            print(fileName)
                                                            
                                                            linkAudio = url.appendingPathComponent("myRcd\(self.audios.count + 1).m4a")
                                                            
                                                            let settings = [
                                                                AVFormatIDKey:Int(kAudioFormatMPEG4AAC),AVSampleRateKey:12000,AVNumberOfChannelsKey:1,AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue]
                                                            
                                                            self.recorder = try! AVAudioRecorder(url: linkAudio!, settings:  settings)
                                                            self.recorder.record()
                                                            let sound = Bundle.main.path(forResource: "Kasih_Ibu_Alto", ofType: "mp3")
                                                            
                                                            self.audioPlayer = try! AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound!))
                                                            self.metronome.toggle()
                                                            self.audioPlayer.play()
                                                            self.audioPlay.toggle()
                                                            metronomeVM.data.isPlaying = true
                                                            self.metronome = true
                                                            
                                                            
                                                        }
                                                        else if description.portType == AVAudioSession.Port.builtInReceiver{
                                                            print("Receiver Output")
                                                            let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                                                            
                                                            let fileName = url.appendingPathComponent("myRcd\(self.audios.count + 1).m4a")
                                                            print(fileName)
                                                            
                                                            linkAudio = url.appendingPathComponent("myRcd\(self.audios.count + 1).m4a")
                                                            
                                                            let settings = [
                                                                AVFormatIDKey:Int(kAudioFormatMPEG4AAC),AVSampleRateKey:12000,AVNumberOfChannelsKey:1,AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue]
                                                            
                                                            self.recorder = try! AVAudioRecorder(url: linkAudio!, settings:  settings)
                                                            self.recorder.record()
                                                            let sound = Bundle.main.path(forResource: "Kasih_Ibu_Alto", ofType: "mp3")
                                                            
                                                            self.audioPlayer = try! AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound!))
                                                            
                                                            self.audioPlayer.play()
                                                            self.audioPlay.toggle()
                                                            metronomeVM.data.isPlaying = true
                                                            metronomeVM.start()
                                                            self.metronome = true
                                                            
                                                            
                                                        }
                                                        else{
                                                            print("Headphone pulled Out")
                                                        }
                                                    }
                                                }else{
                                                    print("Connection to device")
                                                }
                                                if self.record && self.audioPlay{
                                                    isStopRecord = true
                                                    self.recorder.stop()
                                                    self.record.toggle()
                                                    isStopPlayGuide = true
                                                    self.audioPlayer.stop()
                                                    self.audioPlay.toggle()
                                                    metronomeVM.data.isPlaying = false
                                                    metronomeVM.stop()
                                                    
                                                    
                                                }
                                            }
                                            catch _{
                                                
                                            }
                                        }, label: {
                                            VStack{
                                                Image(systemName: "record.circle").font(.system(size: 38)).foregroundColor(.init(Color("PrimaryColor")))
                                                Text("Record").foregroundColor(.pink)
                                            }
                                        })
                                    }
                                    else {
                                        
                                        Button(action: {
                                            
                                            self.selection = 1
                                            
                                            do{
                                                
                                                try! self.audioPlayer.stop()
                                                try! self.recorder.stop()
                                                metronomeVM.data.isPlaying = false
                                                metronomeVM.stop()
                                                self.isStopRecord = false
                                                self.isStopPlayGuide = false
                                                self.record = false
                                                self.audioPlay = false
                                                self.metronome = false
                                                
                                                //print(isStopRecord, isStopPlayGuide, record, audioPlay)
                                            }
                                            catch{
                                                
                                            }
                                            /*
                                             self.isStopRecord = false
                                             self.isStopPlayGuide = false
                                             self.record = false
                                             self.audioPlay = false
                                             */
                                            
                                        }){
                                            VStack{
                                                Image(systemName: "stop.circle").resizable()
                                                    .foregroundColor(Color.pink)
                                                    .frame(width: 43, height: 43)
                                                    .aspectRatio(contentMode: .fit)
                                                Text("Recording").font(.system(size: 12).bold()).foregroundColor(Color.pink)
                                            }
                                        }
                                        
                                        
                                        NavigationLink(destination: EmptyView()) {
                                            EmptyView()
                                        }
                                        
                                        /*
                                         , label: {
                                         NavigationLink(
                                         destination: ScoreFeedbackView(linkAudio: linkAudio!),
                                         isActive: $isTapped,
                                         label: {VStack{
                                         Image(systemName: "stop.circle").resizable()
                                         .foregroundColor(Color.pink)
                                         .frame(width: 43, height: 43)
                                         .aspectRatio(contentMode: .fit)
                                         Text("Recording").font(.system(size: 12).bold()).foregroundColor(Color.pink)
                                         }
                                         
                                         })
                                         NavigationLink(destination: EmptyView()) {
                                         EmptyView()
                                         }
                                         
                                         })
                                         */
                                        
                                    }
                                }
                                
                                
                            }
                            
                            /*
                             NavigationLink(
                             destination: ScoreFeedbackView(),
                             label: {
                             VStack{
                             Image(systemName: "record.circle").font(.system(size: 38)).foregroundColor(.init(Color("PrimaryColor")))
                             Text("Recording").foregroundColor(.red)
                             }
                             })
                             */
                            
                            
                        }
                    }
                }.onReceive(timerAll, perform: { _ in
                    
                    
                }).frame(width: geometry.size.width, height: geometry.size.height, alignment: .center)
                .onAppear(perform: {
                    do{
                        
                        self.nc.addObserver(forName: AVAudioSession.routeChangeNotification, object: nil, queue: nil, using: self.VPNDidChangeStatus(_:))
                        self.session = AVAudioSession.sharedInstance()
                        
                        try self.session.setCategory(.playAndRecord)
                        self.session.requestRecordPermission({(status) in
                            if !status{
                                self.alert.toggle()
                            }
                            else{
                                
                            }
                        })
                    }catch let err{
                        
                    }
                }).onDisappear(perform: {
                    
                    do{
                        self.audioPlayer?.stop()
                        try? self.recorder?.stop()
                        self.metronomeVM.data.isPlaying = false
                        self.metronomeVM.stop()
                        self.isStopRecord = false
                        self.isStopPlayGuide = false
                        self.record = false
                        self.audioPlay = false
                        //self.orientationInfo.rotationDeleteObserver()
                        
                    }
                    catch{
                        
                    }
                    
                })
            }
            else{
                if alert{
                    CustomAlertDialog(showAlert: $alert, width: UIScreen.main.bounds.width  - 100, height: UIScreen.main.bounds.height * 0.3 - 20).frame(width: geometry.size.width, height: geometry.size.height, alignment: .center)
                }
                else{
                    VStack{
                        HStack{
                            Spacer()
                            Text("60 Bpm")
                            Image(systemName: "metronome.fill").font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                            Toggle("", isOn:$metronome).frame(width:60).toggleStyle(SwitchToggleStyle(tint: Color("PrimaryColor"))).scaleEffect(x:0.8,y:0.8)
                        }.padding().frame( height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: .trailing).onChange(of: metronome, perform: { value in
                            if value{
                                metronomeVM.data.isPlaying = value
                                metronomeVM.start()
                            }
                            else{
                                metronomeVM.stop()
                            }
                            
                        }).onDisappear(perform: {
                            metronomeVM.stop()
                        })
                        Image("potrait").frame(width: 352, height:456, alignment: .top)
                        NavigationLink(destination: ScoreFeedbackView(linkAudio: linkAudio),isActive:$navigationScoreView){
                            if isStopRecord == false && isStopPlayGuide == false{
                                if self.record == false && self.audioPlay == false{
                                    Button(action: {
                                        do{
                                            let currentRoute = AVAudioSession.sharedInstance().currentRoute
                                            
                                            if(currentRoute.outputs != nil){
                                                for description in currentRoute.outputs{
                                                    if description.portType == AVAudioSession.Port.headphones{
                                                        print("Headphone plugged in")
                                                        let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                                                        
                                                        let fileName = url.appendingPathComponent("myRcd\(self.audios.count + 1).m4a")
                                                        print(fileName)
                                                        
                                                        linkAudio = url.appendingPathComponent("myRcd\(self.audios.count + 1).m4a")
                                                        
                                                        let settings = [
                                                            AVFormatIDKey:Int(kAudioFormatMPEG4AAC),AVSampleRateKey:12000,AVNumberOfChannelsKey:1,AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue]
                                                        
                                                        self.recorder = try! AVAudioRecorder(url: linkAudio!, settings:  settings)
                                                        self.recorder.record()
                                                        let sound = Bundle.main.path(forResource: "Kasih_Ibu_Alto.mp3", ofType: "mp3")
                                                        
                                                        self.audioPlayer = try! AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound!))
                                                        
                                                        self.audioPlayer.play()
                                                        self.audioPlay.toggle()
                                                        metronomeVM.data.isPlaying = true
                                                        metronomeVM.start()
                                                        self.metronome = true
                                                    }
                                                    else if description.portType == AVAudioSession.Port.displayPort{
                                                        print("Speaker Output")
                                                        let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                                                        
                                                        let fileName = url.appendingPathComponent("myRcd\(self.audios.count + 1).m4a")
                                                        print(fileName)
                                                        
                                                        linkAudio = url.appendingPathComponent("myRcd\(self.audios.count + 1).m4a")
                                                        
                                                        let settings = [
                                                            AVFormatIDKey:Int(kAudioFormatMPEG4AAC),AVSampleRateKey:12000,AVNumberOfChannelsKey:1,AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue]
                                                        
                                                        self.recorder = try! AVAudioRecorder(url: linkAudio!, settings:  settings)
                                                        self.recorder.record()
                                                        let sound = Bundle.main.path(forResource: "Kasih_Ibu_Alto", ofType: "mp3")
                                                        
                                                        self.audioPlayer = try! AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound!))
                                                        
                                                        self.audioPlayer.play()
                                                        self.audioPlay.toggle()
                                                        metronomeVM.data.isPlaying = true
                                                        metronomeVM.start()
                                                        self.metronome = true
                                                        
                                                        
                                                    }
                                                    else if description.portType == AVAudioSession.Port.builtInSpeaker{
                                                        print("Speaker Output")
                                                        let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                                                        
                                                        let fileName = url.appendingPathComponent("myRcd\(self.audios.count + 1).m4a")
                                                        print(fileName)
                                                        
                                                        linkAudio = url.appendingPathComponent("myRcd\(self.audios.count + 1).m4a")
                                                        
                                                        let settings = [
                                                            AVFormatIDKey:Int(kAudioFormatMPEG4AAC),AVSampleRateKey:12000,AVNumberOfChannelsKey:1,AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue]
                                                        
                                                        self.recorder = try! AVAudioRecorder(url: linkAudio!, settings:  settings)
                                                        self.recorder.record()
                                                        let sound = Bundle.main.path(forResource: "Kasih_Ibu_Alto", ofType: "mp3")
                                                        
                                                        self.audioPlayer = try! AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound!))
                                                        self.metronome.toggle()
                                                        self.audioPlayer.play()
                                                        self.audioPlay.toggle()
                                                        metronomeVM.data.isPlaying = true
                                                        self.metronome = true
                                                        
                                                        
                                                    }
                                                    else if description.portType == AVAudioSession.Port.builtInReceiver{
                                                        print("Speaker Output")
                                                        let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                                                        
                                                        let fileName = url.appendingPathComponent("myRcd\(self.audios.count + 1).m4a")
                                                        print(fileName)
                                                        
                                                        linkAudio = url.appendingPathComponent("myRcd\(self.audios.count + 1).m4a")
                                                        
                                                        let settings = [
                                                            AVFormatIDKey:Int(kAudioFormatMPEG4AAC),AVSampleRateKey:12000,AVNumberOfChannelsKey:1,AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue]
                                                        
                                                        self.recorder = try! AVAudioRecorder(url: linkAudio!, settings:  settings)
                                                        self.recorder.record()
                                                        let sound = Bundle.main.path(forResource: "Kasih_Ibu_Alto", ofType: "mp3")
                                                        
                                                        self.audioPlayer = try! AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound!))
                                                        
                                                        self.audioPlayer.play()
                                                        self.audioPlay.toggle()
                                                        metronomeVM.data.isPlaying = true
                                                        metronomeVM.start()
                                                        self.metronome = true
                                                        
                                                        
                                                    }
                                                    else{
                                                        print("Headphone pulled Out")
                                                    }
                                                }
                                            }else{
                                                print("Connection to device")
                                            }
                                            if self.record && self.audioPlay{
                                                isStopRecord = true
                                                self.recorder.stop()
                                                self.record.toggle()
                                                isStopPlayGuide = true
                                                self.audioPlayer.stop()
                                                self.audioPlay.toggle()
                                                
                                                metronomeVM.data.isPlaying = false
                                            }
                                        }
                                        catch _{
                                            
                                        }
                                    }, label: {
                                        VStack{
                                            Image(systemName: "record.circle").font(.system(size: 38)).foregroundColor(.init(Color("PrimaryColor")))
                                            Text("Record").foregroundColor(.pink)
                                        }
                                    })
                                }
                                else {
                                    
                                    Button(action: {
                                        
                                        self.navigationScoreView = true
                                        do{
                                            try! self.audioPlayer.stop()
                                            try! self.recorder.stop()
                                            metronomeVM.data.isPlaying = false
                                            metronomeVM.stop()
                                            self.isStopRecord = false
                                            self.isStopPlayGuide = false
                                            self.record = false
                                            self.audioPlay = false
                                            self.metronome = false
                                            
                                            //print(isStopRecord, isStopPlayGuide, record, audioPlay)
                                        }
                                        catch{
                                            
                                        }
                                        /*
                                         self.isStopRecord = false
                                         self.isStopPlayGuide = false
                                         self.record = false
                                         self.audioPlay = false
                                         */
                                        
                                    }){
                                        VStack{
                                            Image(systemName: "stop.circle").resizable()
                                                .foregroundColor(Color.pink)
                                                .frame(width: 43, height: 43)
                                                .aspectRatio(contentMode: .fit)
                                            Text("Recording").font(.system(size: 12).bold()).foregroundColor(Color.pink)
                                        }
                                    }
                                    
                                    
                                    NavigationLink(destination: EmptyView()) {
                                        EmptyView()
                                    }
                                    
                                    /*
                                     , label: {
                                     NavigationLink(
                                     destination: ScoreFeedbackView(linkAudio: linkAudio!),
                                     isActive: $isTapped,
                                     label: {VStack{
                                     Image(systemName: "stop.circle").resizable()
                                     .foregroundColor(Color.pink)
                                     .frame(width: 43, height: 43)
                                     .aspectRatio(contentMode: .fit)
                                     Text("Recording").font(.system(size: 12).bold()).foregroundColor(Color.pink)
                                     }
                                     
                                     })
                                     NavigationLink(destination: EmptyView()) {
                                     EmptyView()
                                     }
                                     
                                     })
                                     */
                                    
                                }
                            }
                            
                            
                        }
                        
                    }.onReceive(timerAll, perform: { _ in
                        
                        
                    })
                    .onAppear(perform: {
                        do{
                            
                            self.nc.addObserver(forName: AVAudioSession.routeChangeNotification, object: nil, queue: nil, using: self.VPNDidChangeStatus(_:))
                            self.session = AVAudioSession.sharedInstance()
                            try self.session.setCategory(.playAndRecord)
                            self.session.requestRecordPermission({(status) in
                                if !status{
                                    self.alert.toggle()
                                }
                                else{
                                    
                                }
                            })
                        }catch let err{
                            
                        }
                    }).onDisappear(perform: {
                        
                        do{
                            self.audioPlayer?.stop()
                            try? self.recorder?.stop()
                            metronomeVM.data.isPlaying = false
                            metronomeVM.stop()
                            
                            //print("Masuuk")
                            self.isStopRecord = false
                            self.isStopPlayGuide = false
                            self.record = false
                            self.audioPlay = false
                        }
                        catch let err{
                            print(err)
                        }
                        
                    })
                }
                
                
            }
            
        }
        
        
        
        
        
        
    }
    
    
}

struct LiveRecordingView_Previews: PreviewProvider {
    static var previews: some View {
        LiveRecordingView()
    }
}
