//
//  OrientationInfoViewmodel.swift
//  Rechoir
//
//  Created by Chondro Satrio Wibowo on 03/08/21.
//

import Foundation
import SwiftUI

final class OrientationInfoViewmodel:ObservableObject{
    enum Orientation {
        case potrait
        case landscape
    }
    
    @Published var orientation:Orientation
     var observer:NSObjectProtocol?
    
    
    func rotationDeleteObserver(){
        print("Destroy Observer")
        if  observer != nil{
            NotificationCenter.default.removeObserver(observer as Any)
            
        }
    }
    func rotationAddObserver(){
        print("Add Observer")
        self.observer = NotificationCenter.default.addObserver(forName: UIDevice.orientationDidChangeNotification, object: nil, queue: nil){
            [ self] note in
            guard let device = note.object as? UIDevice else{
                return
            }
            if device.orientation.isPortrait{
                self.orientation = .potrait
            }
            else if device.orientation.isLandscape{
                self.orientation = .landscape
            }
        }
    }
    init() {
        if UIDevice.current.orientation.isLandscape{
            self.orientation = .landscape
        }
        else{
            self.orientation = .potrait
        }
        
        
    }
    deinit {
        
    }
}
