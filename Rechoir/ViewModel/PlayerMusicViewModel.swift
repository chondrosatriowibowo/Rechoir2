//
//  PlayerMusicViewModel.swift
//  Rechoir
//
//  Created by Chondro Satrio Wibowo on 26/07/21.
//

import AudioKit
import Foundation
import AudioKitEX
import STKAudioKit
struct ShakerMetronomeData{
    var isPlaying = false;
    var tempo:BPM = 60
    var timeSignatureTop:Int = 4
    var downBeatNoteNumber = MIDINoteNumber(6)
    var beatNoteNuber = MIDINoteNumber(10)
    
    var NoteVelocity = 127.0
    var currentBeat = 0
    
    
}
class PlayerMusicViewModel:ObservableObject{
    let engine = AudioEngine()
    let shaker = Shaker()
    var callbackInst = CallbackInstrument()
    let reverb:Reverb
    let mixer = Mixer()
    var sequencer = Sequencer()
    
    @Published var data = ShakerMetronomeData(){
        didSet{
            data.isPlaying ? sequencer.play() : sequencer.stop()
            sequencer.tempo = data.tempo
            
        }
    }
    func updateSequences(){
        var track = sequencer.tracks.first!
        track.length = Double(data.timeSignatureTop)
        
        track.clear()
        
        let vel = MIDIVelocity(Int(data.NoteVelocity))
        for beat in 1 ..< data.timeSignatureTop{
            track.sequence.add(noteNumber: data.beatNoteNuber, velocity: vel,position: Double(beat), duration: 1)
        }
        track = sequencer.tracks[1]
        track.length = Double(data.timeSignatureTop)
        track.clear()
        
        for beat in 0 ..< data.timeSignatureTop{
            track.sequence.add(noteNumber: MIDINoteNumber(beat),position: Double(beat), duration: 1)
        }
    }
    init() {
        let fader = Fader(shaker)
        fader.gain = 20.0
        reverb = Reverb(fader)
        
        let _ = sequencer.addTrack(for: shaker)
        
        callbackInst = CallbackInstrument(midiCallback:{(_,beat,_)in
            self.data.currentBeat = Int(beat)
            print(beat)
        })
        
        let _ = sequencer.addTrack(for: shaker)
        updateSequences()
        
        mixer.addInput(reverb)
        mixer.addInput(callbackInst)
        
        engine.output = mixer
    }
    func start(){
        do{
            try engine.start();
        }catch let err{
            Log(err)
        }
    }
    func stop(){
        
        sequencer.stop()
        engine.stop()
    }
}
