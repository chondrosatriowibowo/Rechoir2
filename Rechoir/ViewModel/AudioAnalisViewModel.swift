//
//  AudioAnalisViewModel.swift
//  Rechoir
//
//  Created by Chondro Satrio Wibowo on 17/07/21.


import AudioKit
import SoundpipeAudioKit
import CSoundpipeAudioKit
import AudioKitEX
import AudioToolbox
import AVFoundation
import Foundation


struct RecorderData {
    var isRecording = false
    var isPlaying = false
}
class TunerData{
    var pitch:Float = 0.0
    var amplitude:Float = 0.0
    var noteNameWithSharps = "-"
    var noteNameWithFlats = "-"
}
class AudioAnalys:ObservableObject{
    let engine = AudioEngine()
    var mic: AudioEngine.InputNode
    var tappableNode1:Fader
    var tappableNodeA:Fader
    var tappableNode2:Fader
    var tappableNodeB:Fader
    var tappableNode3:Fader
    var tappableNodeC:Fader
    
    let player = AudioPlayer()
    var tracker:PitchTap!
    var silence:Fader
    var recorder:NodeRecorder?
    let mixer = Mixer()
    let noteFrequencies = [16.35,17.32,18.35,19.45,20.6,21.83,23.12,24.5,25.96,27.5,29.14,30.87,32.70,34.65,36.71,38.89,41.20,43.65,46.25,49.00,51.91,55.00,58.27,61.74,65.41,69.30,73.42,77.78,82.41,87.31,92.50,98.00,103.83,110.00,116.54,123.47,130.81,138.59,146.83,155.56,164.81,174.61,185.00,196.00,207.65,220.00,233.08,246.94,261.63,277.18,293.66,311.13,329.63,349.23,369.99,392.00,415.30,440.00,466.16,493.88,523.25]
    let noteNameWithSharps = ["C","C#","D","D#","E","F","F#","G","G#","A","A#","B","C1","C1#","D1","D1#","E1","F1","F1#","G1","G1#","A1","A1#","B1","C2","C2#","D2","D2#","E2","F2","F2#","G2","G2#","A2","A2#","B2","C3","C3#","D3","D3#","E3","F3","F3#","G3","G3#","A3","A3#","B3","C4","C4#","D4","D4#","E4","F4","F4#","G4","G4#","A4","A4#","B4","C5"]
    let noteNameWithFlats = ["C","D♭","D","E♭","E","F","G♭","G","A♭","A","B♭","B","C1","D1♭","D1","E1♭","E1","F1","G1♭","G1","A1♭","A1","B1♭","B1","C2","D2♭","D2","E2♭","E2","F2","G2♭","G2","A2♭","A2","B2♭","B2","C3","D3♭","D3","E3♭","E3","F3","G3♭","G3","A3♭","A3","B3♭","B3","C4","D4♭","D4","E4♭","E4","F4","G4♭","G4","A4♭","A4","B4♭","B4","C5"]
    
    @Published var data = TunerData()
    @Published var pitch = ""
    @Published var amp = ""
    
    @Published var recording = RecorderData(){
        didSet{
            if recording.isRecording{
                NodeRecorder.removeTempFiles()
                do{
                    try recorder?.record()
                }catch let err{
                    print(err)
                }
            }else{
                recorder?.stop()
            }
            
            if recording.isPlaying{
                if let file = recorder?.audioFile{
                    player.file = file
                    player.play()
                }
            }else{
                player.stop()
            }
        }
    }
    
    func update(_ pitch:AUValue,_ amp:AUValue){
        
        data.pitch = pitch
        print(pitch)
        data.amplitude = amp
        print(amp)
        
        var frequency = pitch
        
        while frequency > Float(noteFrequencies[noteFrequencies.count - 1]) {
            frequency /= 2.0
        }
        while frequency < Float(noteFrequencies[0]) {
            frequency *= 2.0
        }
        
        var minDistance:Float = 10000
        var index = 0
        
        for possibleIndex in 0 ..< noteFrequencies.count{
            let distance = fabsf(Float(noteFrequencies[possibleIndex]) - frequency)
            if distance < minDistance{
                index = possibleIndex
                minDistance = distance
            }
        }
        let octave = Int(log2f(pitch / frequency))
        self.pitch = "\(noteNameWithSharps[index])\(octave)"
        self.amp = "\(noteNameWithFlats[index])\(octave)"
        
    }
    init (){
       
       guard let input = engine.input else {
          fatalError()
       }
        do{
            recorder = try NodeRecorder(node: input)
        }
        catch let err{
            fatalError("\(err)")
        }
       mic = input
       tappableNode1 = Fader(self.mic)
       tappableNode2 = Fader(self.tappableNode1)
       tappableNode3 = Fader(self.tappableNode2)
       tappableNodeA = Fader(self.tappableNode3)
       tappableNodeB = Fader(self.tappableNodeA)
       tappableNodeC = Fader(self.tappableNodeB)
       silence = Fader(self.tappableNodeC,gain: 0)
        mixer.addInput(silence)
        
       
       engine.output = silence
       tracker = PitchTap(mic){pitch,amp in
           DispatchQueue.main.async {
               self.update(pitch[0], amp[0])
           }
       }
       
   }
    func start(){
        do{
            try engine.start()
            tracker.start()
        }
        catch let err{
            Log(err)
        }
        
    }
    
    
    func test(){
        let osc = Oscillator(amplitude:0.4)
        
        engine.output = osc
        
        try! engine.start()

        while true {
            osc.frequency = Float.random(in: 100...200)
            osc.play()
            sleep(1)
            osc.stop()
            sleep(1)
        }

        
    }
    func play(){
        
        
        
        try! engine.start();
       
        
      
    }
    
    func stop(){
        let oscilator = Oscillator(waveform:Table(.triangle),frequency:440)
        engine.output = oscilator
        oscilator.$frequency.ramp(to: 660, duration: 1)
        var pitches:[Float] = []
        let knownValues:[Float] = [100.0, 448.91534, 457.72495, 476.3929, 503.23022, 519.6525, 533.7021, 562.2199, 576.985, 598.7911]
        
        let tap = PitchTap(oscilator){(tapPitches,_)in
            pitches.append(tapPitches[0])
            
            if pitches.count == knownValues.count{
                
            }
            
        }
        if tap.isStarted{
            tap.stop()
        }
    }
}

