//
//  ContentView.swift
//  Rechoir
//
//  Created by Chondro Satrio Wibowo on 15/07/21.
//

import SwiftUI

struct ContentView: View {
    
    @StateObject var conductor = AudioAnalys()
    var body: some View {
        VStack{
            Text("Frequency")
            Spacer()
            Text("\(conductor.data.pitch,specifier:"%0.1f")")
            Text("Note Name")
            
            Text("\(conductor.pitch) / \(conductor.amp)")
            Spacer()
            Button(action: {
                conductor.start()
            }, label: {
                Image(systemName: "play")
            })
            
            
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
