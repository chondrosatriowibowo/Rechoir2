//
//  RecordComponents.swift
//  Rechoir
//
//  Created by Chondro Satrio Wibowo on 27/07/21.
//

import SwiftUI

struct RecordComponents: View {
    var body: some View {
        VStack(alignment:.leading){
            HStack{
                Image("kasihibumusic").resizable().frame(width:120,height:120).cornerRadius(12)
                
                VStack(alignment:.leading){
                    Text("Part 1").fontWeight(.semibold).multilineTextAlignment(/*@START_MENU_TOKEN@*/.leading/*@END_MENU_TOKEN@*/)
                    Rectangle().frame(height:40).foregroundColor(.white)
                    Text("Unfinished").multilineTextAlignment(/*@START_MENU_TOKEN@*/.leading/*@END_MENU_TOKEN@*/)
                }.frame(width:100,alignment: .leading).multilineTextAlignment(/*@START_MENU_TOKEN@*/.leading/*@END_MENU_TOKEN@*/)
                Spacer()
                
            }.frame(height:150).padding()
        }
        
    }
}

struct RecordComponents_Previews: PreviewProvider {
    static var previews: some View {
        RecordComponents()
    }
}
