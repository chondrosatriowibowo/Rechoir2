//
//  CustomAlertDialog.swift
//  Rechoir
//
//  Created by Chondro Satrio Wibowo on 03/08/21.
//

import SwiftUI

struct CustomAlertDialog: View {
    @Binding var showAlert:Bool;
    var width:CGFloat
    var height:CGFloat
    var body: some View {
        VStack{
            Image(systemName: "headphones").foregroundColor(.init(Color("PrimaryColor"))).font(.system(size: 42))
            Rectangle().frame(height:2).foregroundColor(.gray.opacity(0))
            Text("Headphone Required").font(.title3).fontWeight(.semibold)
                
            Rectangle().frame(height:4).foregroundColor(.gray.opacity(0))
            Text("Please use headphones so this app can detect sounds more accurately").font(.system(size: 15)).frame(width:250)
            Divider()
            Button(action: {showAlert.toggle()}, label: {
                Text("OK").font(.system(size: 17)).fontWeight(.semibold)
            }).frame(width: UIScreen.main.bounds.width - 100, height: 30, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            
        }.frame(width: width, height: height, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).background(RoundedRectangle(cornerRadius: /*@START_MENU_TOKEN@*/25.0/*@END_MENU_TOKEN@*/, style: /*@START_MENU_TOKEN@*/.continuous/*@END_MENU_TOKEN@*/).foregroundColor(.init(Color("BackgroundAlert"))).shadow(color: /*@START_MENU_TOKEN@*/.black/*@END_MENU_TOKEN@*/.opacity(0.4), radius: 5, x: 8, y: 8))
    }
}

struct CustomAlertDialog_Previews: PreviewProvider {
    static var previews: some View {
        CustomAlertDialog(showAlert: .constant(true),width: 100,height: 100)
    }
}
