//
//  HarmonyComponents.swift
//  Rechoir
//
//  Created by Chondro Satrio Wibowo on 27/07/21.
//

import SwiftUI

struct HarmonyComponents: View {
    var text:String
    var body: some View {
        HStack{
            Text(text)
            Spacer()
            
        }.padding()
    }
}

struct HarmonyComponents_Previews: PreviewProvider {
    static var previews: some View {
        HarmonyComponents(text: "Sopran")
    }
}
