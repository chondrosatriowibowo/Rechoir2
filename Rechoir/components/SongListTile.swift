//
//  SongListTile.swift
//  Rechoir
//
//  Created by Chondro Satrio Wibowo on 26/07/21.
//

import SwiftUI

struct SongListTile: View {
    var name:String
    var time:String
    var body: some View {
        HStack{
            VStack(alignment:.leading){
                Text(name).font(.title2).foregroundColor(.white).fontWeight(.semibold)
                Text(time).font(.custom( "Arial Rounded MT Bold", size: 12)).foregroundColor(.white)
            }
            
            Spacer()
            Image(systemName: "chevron.forward").foregroundColor(.white)
        }.padding(.vertical,10).padding(.horizontal,20).background(RoundedRectangle(cornerRadius: 12.0, style: /*@START_MENU_TOKEN@*/.continuous/*@END_MENU_TOKEN@*/).padding(.horizontal,10))
    }
}

struct SongListTile_Previews: PreviewProvider {
    static var previews: some View {
        SongListTile(name: "Kasih Bapak", time: "0:33")
    }
}
