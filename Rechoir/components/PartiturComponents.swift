//
//  PartiturComponents.swift
//  Rechoir
//
//  Created by Chondro Satrio Wibowo on 27/07/21.
//

import SwiftUI

struct PartiturComponents: View {
    var body: some View {
        ZStack(alignment:.center){
            Image("lyrics")
            Rectangle().foregroundColor(.init( red: 0.5, green: 0.5, blue: 0.5, opacity: 0.4))
        }.frame(height:150)
    }
}

struct PartiturComponents_Previews: PreviewProvider {
    static var previews: some View {
        PartiturComponents()
    }
}
