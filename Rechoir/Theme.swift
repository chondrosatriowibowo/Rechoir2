//
//  Theme.swift
//  Rechoir
//
//  Created by Chondro Satrio Wibowo on 26/07/21.
//

import Foundation
import SwiftUI
class Theme{
    static func navigationBarColors(background:UIColor?,titleColor:UIColor? = nil,tintColor:UIColor? = nil){
        
        let navigationApparance = UINavigationBarAppearance()
        navigationApparance.configureWithOpaqueBackground()
        navigationApparance.backgroundColor = background ?? .clear
        
        navigationApparance.titleTextAttributes = [.foregroundColor:titleColor ?? .black]
        navigationApparance.largeTitleTextAttributes = [.foregroundColor:titleColor ?? .black]
        
        UINavigationBar.appearance().standardAppearance = navigationApparance
        UINavigationBar.appearance().compactAppearance = navigationApparance
        UINavigationBar.appearance().scrollEdgeAppearance = navigationApparance
        
        UINavigationBar.appearance().tintColor = tintColor ?? titleColor ?? .black
    }
}
